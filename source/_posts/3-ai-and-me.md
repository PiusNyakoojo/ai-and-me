---
title: 3 - About AI and You
post_type: blog
date: 2019-01-08 20:00:00
order_index: 3
short_description: A tool for training AI to do tasks on the computer
---

A tool for training AI to do tasks on the computer

### TeX (LaTeX)

$$E=mc^2$$

Inline ($$E=mc^2$$) Inline，Inline E=mc^2 Inline。

$$(\sqrt{3x-1}+(1+x)^2)$$

$$(\f{x} = \int_{-\infty}^\infty
    \hat \f\xi\,e^{2 \pi i \xi x}
    \,d\xi)$$


$$\f{x} = 3$$
$$\sin(\alpha)^{\theta}=\sum_{i=0}^{n}(x^i + \cos(f))$$

### FlowChart
