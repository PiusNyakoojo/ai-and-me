---
title: AI and Me
type_name: blog
---

### Here's how the project structure develops.

Coming from a background in web development and a passion for artificial intelligence (AI) algorithms, here's where I provide insight into my career, my passions and my life.

AI and Me is an initiative to teach people about the coming era of people living with advanced artificial intelligence (AI). First, we will introduce the reasons why we can expect these advancements to take place within the next few years.

Following the introduction, we will talk about the "Me" in "AI and Me". Me, or really, you might want to know more about life with AI-enabled systems. If you have a technical background, you will also want to continue reading. If your background is less focused on the sciences, you are also perfectly fit to be here.

Following the section about people (you and me), we spark a discussion around artificial intelligence.

With the "Me" and AI sections taken care of, we can finally reason about how both systems can fruitfully interact with one another in the coming years.

"The Life Together" brings everything we've learned together and gives glimpses and examples of interactions that take place in an era of advanced AI.

"Preparing for the Life" provides examples of things you can do to adjust your current life to the life with AI systems.

Finally, we end with a new beginning: "Beyond the Life" which discusses the future beyond the future.
