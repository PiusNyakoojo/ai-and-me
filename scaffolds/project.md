---
title: {{ title }}
date: {{ date }}
post_type: project
project_type: {{ type }}
short_description: {{ description }}
---
